package problem23;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testBin2Dec() {
        assertEquals("Expected value: 10", App.bin2Dec("1010"), 10);
        assertEquals("Expected value: 7", App.bin2Dec("111"), 7);
        assertEquals("Expected value: 331345", App.bin2Dec("1010000111001010001"), 331345);
        assertEquals("Expected value: 0", App.bin2Dec(""), 0);
    }
}
