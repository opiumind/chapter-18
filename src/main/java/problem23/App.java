package problem23;

import java.util.Scanner;

/**
 * (Binary to decimal) Write a recursive method that parses a binary number as a
 string into a decimal integer. The method header is:

 public static int bin2Dec(String binaryString)

 Write a test program that prompts the user to enter a binary string and displays
 its decimal equivalent.
 *
 */
public class App 
{

    public static int bin2Dec(String binaryString) {
        int length = binaryString.length();

        if (length == 0) {
            return 0;
        }

        String current = binaryString.substring(0,1);
        String rest = binaryString.substring(1);

        return Integer.parseInt(current) * (int)Math.pow(2, length - 1) + bin2Dec(rest);
    }

    private static String removeZeroesInBeginning(String str) {
        if (str.length() == 1 && str.charAt(0) == '0') {
            return str;
        }
        if (str.length() > 0 && str.charAt(0) == '0') {
            return removeZeroesInBeginning(str.substring(1));
        } else {
            return str;
        }
    }

    public static void main( String[] args )
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter binary string:");
        String enteredStr = input.nextLine();

        if (removeZeroesInBeginning(enteredStr).matches("(0|1){1,31}")) {
            System.out.println(bin2Dec(removeZeroesInBeginning(enteredStr)));
        } else {
            System.out.println("Incorrect input: you should enter binary value which contains only 0 and 1 " +
                    "and doesn't exceed 31 symbols");
        }
    }
}
