var assert = require('assert');
var bin2dec = require('../chapter18');

describe('Converting binary value into decimal', function() {
  it('expected 7', function () {
    assert.equal(7, bin2dec.bin2Dec("111"));
  });
});