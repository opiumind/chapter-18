function bin2Dec(binaryString) {
  var length = binaryString.length;

  if (length === 0) {
    return 0;
  }

  var current = binaryString.substring(0,1);
  var rest = binaryString.substring(1);

  return current * Math.pow(2,(length - 1)) + bin2Dec(rest);
}

function removeZeroesInBeginning(arg) {
  var str = arg + "";
  if (str.length === 1 && str.charAt(0) === '0') {
    return str;
  }

  if (str.length > 0 && str.charAt(0) === '0') {
    return removeZeroesInBeginning(str.substring(1));
  } else {
    return str;
  }
}

var arg = process.argv.slice(2);
if (removeZeroesInBeginning(arg).match(/^[01]{1,31}$/g)) {
  console.log(bin2Dec(removeZeroesInBeginning(arg)));
} else {
  console.log("Incorrect input: you should enter binary value which contains only 0 and 1 " +
    "and doesn't exceed 31 symbols");
}

module.exports = {
  bin2Dec: bin2Dec
};